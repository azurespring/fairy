package huobi

import (
	"compress/gzip"
	"encoding/json"
	"github.com/gorilla/websocket"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestNewStreams(t *testing.T) {
	saved := marketWSS
	defer func() { marketWSS = saved }()

	upgrader := websocket.Upgrader{
		ReadBufferSize:  4096,
		WriteBufferSize: 4096,
	}

	ch := make(chan *websocket.Conn)
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		conn, _ := upgrader.Upgrade(w, r, nil)
		ch <- conn
	}))
	defer srv.Close()

	marketWSS = "ws://" + strings.TrimPrefix(srv.URL, "http://")
	streams, err := NewStreams()
	if err != nil {
		t.Error(err)
	}

	conn := <-ch
	w, _ := conn.NextWriter(websocket.BinaryMessage)
	z := gzip.NewWriter(w)
	json.NewEncoder(z).Encode(ping{1523443161})
	z.Close()
	w.Close()

	_, r, _ := conn.NextReader()
	var p pong
	if err := json.NewDecoder(r).Decode(&p); err != nil {
		t.Error(err)
	}
	if p.Pong != 1523443161 {
		t.Errorf("p.pong = %d, want %d", p.Pong, 1523443161)
	}

	conn.Close()
	if err := streams.Wait(); err == nil {
		t.Error("want an error")
	}
}
