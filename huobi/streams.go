package huobi

import (
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"io/ioutil"
)

var (
	StreamsClosedError = errors.New("streams closed")
	endOfStreams = errors.New("end of streams")

	marketWSS = "wss://api.huobipro.com/ws"
)

type ping struct {
	Ping int64 `json:"ping"`
}

type pong struct {
	Pong int64 `json:"pong"`
}

type topic interface {
	fmt.Stringer
}

type wsBundle struct {
	b   []byte
	err error
}

type wire struct {
	Ch   string
	Ts   Timestamp
	Tick json.RawMessage
}

type broker struct {
	producer
	cs []consumer
}

type Streams struct {
	conn *websocket.Conn
	err  chan error
	end  chan struct{}
	cmd  chan command
	bus  chan []byte
	reqs map[string]chan<- wsBundle
	brks map[string]*broker
}

func NewStreams() (*Streams, error) {
	conn, _, err := (&websocket.Dialer{EnableCompression: true}).Dial(marketWSS, nil)
	if err != nil {
		return nil, err
	}

	s := Streams{
		conn,
		make(chan error, 1),
		make(chan struct{}, 5),
		make(chan command, 5),
		make(chan []byte, 5),
		map[string]chan<- wsBundle{},
		map[string]*broker{},
	}
	s.start()

	return &s, nil
}

func (s *Streams) start() {
	go s.readLoop()
	go s.evalLoop()
}

func (s *Streams) Wait() error {
	<-s.end

	select {
	case err := <-s.err:
		return err

	default:
		return nil
	}
}

func (s *Streams) Close() {
	s.conn.Close()
}

func (s *Streams) error(err error) {
	select {
	case s.err <- err:
	default:
	}

	s.conn.Close()
	close(s.end)
}

func (s *Streams) readLoop() {
	for {
		_, r, err := s.conn.NextReader()
		if err != nil {
			s.error(err)

			break
		}

		z, err := gzip.NewReader(r)
		if err != nil {
			s.error(err)

			break
		}

		b, err := ioutil.ReadAll(z)
		z.Close()
		if err != nil {
			s.error(err)

			break
		}

		s.bus <- b
	}
}

func write(conn *websocket.Conn, v interface{}) error {
	w, err := conn.NextWriter(websocket.TextMessage)
	if err != nil {
		return err
	}

	err = json.NewEncoder(w).Encode(v)
	w.Close()

	return err
}

func (s *Streams) evalLoop() {
	var err error
	for err == nil {
		select {
		case <-s.end:
			err = endOfStreams

		case b := <-s.bus:
			err = s.onRecv(b)

		case cmd := <-s.cmd:
			err = cmd.exec(s)
		}
	}
	if err != endOfStreams {
		s.error(err)
	}

	for _, ch := range s.reqs {
		ch <- wsBundle{nil, StreamsClosedError}
	}
	for _, brk := range s.brks {
		for _, c := range brk.cs {
			c.close()
		}
	}
}

func (s *Streams) onRecv(b []byte) error {
	var err error
	for _, f := range []func([]byte) ([]byte, error){s.onPing, s.onWire, s.onAck} {
		b, err = f(b)
		if err != nil {
			return err
		}
		if b == nil {
			break
		}
	}

	return nil
}

func (s *Streams) onPing(b []byte) ([]byte, error) {
	var p ping
	if err := json.Unmarshal(b, &p); err != nil {
		return b, err
	}
	if p.Ping == 0 {
		return b, nil
	}
	if err := write(s.conn, pong{p.Ping}); err != nil {
		return nil, err
	}

	return nil, nil
}

func (s *Streams) onWire(b []byte) ([]byte, error) {
	var wr wire
	if err := json.Unmarshal(b, &wr); err != nil {
		return b, err
	}
	if wr.Ch == "" {
		return b, nil
	}

	if brk, ok := s.brks[wr.Ch]; ok {
		v := brk.get()
		if err := json.Unmarshal(wr.Tick, v); err != nil {
			return nil, err
		}

		var cs []consumer
		for _, c := range brk.cs {
			if c.put(v) != nil {
				cs = append(cs, c)
			} else {
				c.close()
			}
		}
		brk.cs = cs
	}

	return nil, nil
}

func (s *Streams) onAck(b []byte) ([]byte, error) {
	var st statusLine
	if err := json.Unmarshal(b, &st); err != nil {
		return b, err
	}

	switch st.Status {
	case stOk:
		s.reqs[st.Id] <- wsBundle{b, nil}

	default:
		var e RestError
		if err := json.Unmarshal(b, &e); err != nil {
			return nil, err
		}

		s.reqs[st.Id] <- wsBundle{nil, e}
	}
	delete(s.reqs, st.Id)

	return nil, nil
}

func (s *Streams) request(topic fmt.Stringer, v interface{}) error {
	ch := make(chan wsBundle)
	select {
	case <-s.end:
		return StreamsClosedError

	case s.cmd <- request{topic, ch}:
		ack := <-ch
		if ack.err != nil {
			return ack.err
		}

		if err := unbundle(ack.b, v); err != nil {
			return err
		}

		return nil
	}
}

type producer interface {
	get() interface{}
}

type consumer interface {
	put(v interface{}) interface{}
	close()
}

type duplex interface {
	producer
	consumer
}

func (s *Streams) subscribe(t topic, dup duplex) error {
	ch := make(chan error)
	select {
	case <-s.end:
		return StreamsClosedError

	case s.cmd <- subscription{t, ch, dup}:
		return <-ch
	}
}

func (s *Streams) unsubscribe(t topic, c consumer) {
	select {
	case <-s.end:
	case s.cmd <- unsubscription{t, c}:
	}
}
