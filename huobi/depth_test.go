package huobi

import (
	"testing"
	"encoding/json"
	"bitbucket.org/azurespring/fab/math"
)

func TestPriceLevel_UnmarshalJSON(t *testing.T) {
	var l PriceLevel
	if err := json.Unmarshal([]byte(`[1885.0000,21.8804]`), &l); err != nil {
		t.Error(err)
	}

	if math.Epsilon(1e-9).Cmp(l.Price, 1885.0000) != 0 {
		t.Errorf("l.Price = %f, want %f", l.Price, 1885.0000)
	}
	if math.Epsilon(1e-9).Cmp(l.Quantity, 21.8804) != 0 {
		t.Errorf("l.Price = %f, want %f", l.Quantity, 21.8804)
	}
}
