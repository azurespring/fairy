package huobi

import "fmt"

type RestError struct {
	Code    string `json:"err-code"`
	Message string `json:"err-msg"`
}

func (e RestError) Error() string {
	return fmt.Sprintf("%s: %s", e.Code, e.Message)
}
