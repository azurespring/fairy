package huobi

import (
	"encoding/json"
	"testing"
	"time"
)

func TestTimestamp_UnmarshalJSON(t *testing.T) {
	var timestamp Timestamp

	if err := json.Unmarshal([]byte(`1494900087029`), &timestamp); err != nil {
		t.Error(err)
	}

	if timestamp.UnixNano() != 1494900087029*int64(time.Millisecond) {
		t.Errorf("timestamp.UnixNano() = %d, want %d", timestamp.UnixNano(), 1494900087029*int64(time.Millisecond))
	}
}
