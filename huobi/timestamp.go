package huobi

import (
	"encoding/json"
	"time"
)

type Timestamp struct {
	time.Time
}

func (t *Timestamp) UnmarshalJSON(data []byte) error {
	var timestamp int64
	if err := json.Unmarshal(data, &timestamp); err != nil {
		return err
	}

	t.Time = time.Unix(
		timestamp/int64(time.Second/time.Millisecond),
		timestamp%int64(time.Second/time.Millisecond)*int64(time.Millisecond),
	)

	return nil
}
