package huobi

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"fmt"
)

type status string

const (
	stOk    status = "ok"
	stError status = "error"
)

type statusLine struct {
	Id     string
	Ts     Timestamp
	Status status
}

type bundle struct {
	Ch   string
	Data json.RawMessage
	Tick json.RawMessage
}

func (b bundle) scoop() json.RawMessage {
	if b.Data != nil {
		return b.Data
	}

	return b.Tick
}

func newBundle(data []byte) (*bundle, error) {
	var s statusLine
	if err := json.Unmarshal(data, &s); err != nil {
		return nil, err
	}
	if s.Status != stOk {
		var e RestError
		if err := json.Unmarshal(data, &e); err != nil {
			return nil, err
		}

		return nil, e
	}

	var b bundle
	if err := json.Unmarshal(data, &b); err != nil {
		return nil, err
	}

	return &b, nil
}

func unbundle(data []byte, v interface{}) error {
	b, err := newBundle(data)
	if err != nil {
		return err
	}

	return json.Unmarshal([]byte(b.scoop()), v)
}

type Tick struct {
	Id     int64
	Open   float64
	Close  float64
	High   float64
	Low    float64
	Amount float64
	Count  int64
	Volume float64 `json:"vol"`
	Ask    *PriceLevel
	Bid    *PriceLevel
}

var (
	marketURL = "https://api.huobipro.com/market"
	tickURL   = marketURL + "/detail"
)

func GetTick(symbol string, ctx context.Context) (*Tick, error) {
	req, err := http.NewRequest(http.MethodGet, tickURL, nil)
	if err != nil {
		return nil, err
	}

	req.URL.RawQuery = url.Values{"symbol": {symbol}}.Encode()
	req = req.WithContext(ctx)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	var t Tick
	if err = unbundle(b, &t); err != nil {
		return nil, err
	}

	return &t, nil
}

type tTick struct {
	symbol string
}

func (t tTick) String() string {
	return fmt.Sprintf("market.%s.detail", t.symbol)
}

func (s *Streams) GetTick(symbol string) (*Tick, error) {
	var t Tick
	if err := s.request(tTick{symbol}, &t); err != nil {
		return nil, err
	}

	return &t, nil
}
