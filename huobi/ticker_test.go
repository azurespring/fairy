package huobi

import (
	"bitbucket.org/azurespring/fab/math"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"compress/gzip"
	"fmt"
	"strings"
)

func TestNewBundle(t *testing.T) {
	var (
		b   *bundle
		err error
	)

	b, err = newBundle([]byte(`malformed json`))
	if err == nil {
		t.Error("want an error")
	}

	if _, err := newBundle([]byte(`
{
  "ts": 1490759594752,
  "status": "error",
  "err-code": "invalid-parameter",
  "err-msg": "invalid symbol"
}
`)); err == nil {
		t.Error("want an error")
	} else if err, ok := err.(RestError); !ok {
		t.Error("want a RestError")
	} else if want := (RestError{"invalid-parameter", "invalid symbol"}); err != want {
		t.Errorf("err = %q, want %q", err, want)
	}

	b, err = newBundle([]byte(`
{
  "status": "ok",
  "ch": "market.btcusdt.detail",
  "ts": 1489473538996,
  "tick": {
    "amount": 4316.4346,
    "open": 8090.54,
    "close": 7962.62,
    "high": 8119.00,
    "ts": 1489464451000,
    "id": 1489464451,
    "count": 9595,
    "low": 7875.00,
    "vol": 34497276.905760
  }
}
`))
	if err != nil {
		t.Error(err)
	}
	if b.Ch != "market.btcusdt.detail" {
		t.Errorf("b.Ch = %q, want %q", b.Ch, "market.btcusdt.detail")
	}
}

func TestUnbundle(t *testing.T) {
	var tick Tick
	if err := unbundle([]byte(`
{
  "status": "ok",
  "ch": "market.btcusdt.detail",
  "ts": 1489473538996,
  "tick": {
    "amount": 4316.4346,
    "open": 8090.54,
    "close": 7962.62,
    "high": 8119.00,
    "ts": 1489464451000,
    "id": 1489464451,
    "count": 9595,
    "low": 7875.00,
    "vol": 34497276.905760
  }
}
`),
		&tick); err != nil {
		t.Error(err)
	}

	if tick.Id != 1489464451 {
		t.Errorf("tick.Id = %d, want %d", tick.Id, 1489464451)
	}
}

func TestGetTick(t *testing.T) {
	saved := tickURL
	defer func() { tickURL = saved }()

	var (
		method string
		symbol string
	)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	srvOk := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		method = r.Method
		symbol = r.FormValue("symbol")

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(`
{
  "status": "ok",
  "ch": "market.btcusdt.detail",
  "ts": 1489473538996,
  "tick": {
    "amount": 4316.4346,
    "open": 8090.54,
    "close": 7962.62,
    "high": 8119.00,
    "ts": 1489464451000,
    "id": 1489464451,
    "count": 9595,
    "low": 7875.00,
    "vol": 34497276.905760
  }
}
`))
	}))
	defer srvOk.Close()

	tickURL = srvOk.URL
	tick, err := GetTick("btcusdt", ctx)
	if err != nil {
		t.Error(err)
	}

	testInt64 := func(a, b interface{}) bool {
		return a.(int64) == b.(int64)
	}
	testString := func(a, b interface{}) bool {
		return a.(string) == b.(string)
	}
	testFloat64 := func(a, b interface{}) bool {
		return math.Epsilon(1e-9).Cmp(a.(float64), b.(float64)) == 0
	}
	for _, check := range []struct {
		val  interface{}
		want interface{}
		test func(a, b interface{}) bool
		fmt  string
	}{
		{tick.Id, int64(1489464451), testInt64, "tick.Id = %d, want %d"},
		{method, http.MethodGet, testString, "method = %s, want %s"},
		{symbol, "btcusdt", testString, "symbol = %s, want %s"},
		{tick.Open, 8090.54, testFloat64, "tick.Open = %f, want %f"},
		{tick.Close, 7962.62, testFloat64, "tick.Close = %f, want %f"},
		{tick.High, 8119.00, testFloat64, "tick.High = %f, want %f"},
		{tick.Low, 7875.00, testFloat64, "tick.Low = %f, want %f"},
		{tick.Amount, 4316.4346, testFloat64, "tick.Amount = %f, want %f"},
		{tick.Count, int64(9595), testInt64, "tick.Count = %d, want %d"},
		{tick.Volume, 34497276.905760, testFloat64, "tick.Volume = %f, want %f"},
	} {
		if !check.test(check.val, check.want) {
			t.Errorf(check.fmt, check.val, check.want)
		}
	}

	srvError := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		method = r.Method
		symbol = r.FormValue("symbol")

		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(`
{
  "ts": 1490759594752,
  "status": "error",
  "err-code": "invalid-parameter",
  "err-msg": "invalid symbol"
}
`))
	}))
	defer srvError.Close()

	tickURL = srvError.URL
	if _, err := GetTick("not-exists", ctx); err == nil {
		t.Error("want an error")
	} else if err, ok := err.(RestError); !ok {
		t.Error("want a RestError")
	} else if want := (RestError{"invalid-parameter", "invalid symbol"}); err != want {
		t.Errorf("err = %q, want %q", err, want)
	}
}

func TestStreams_GetTick(t *testing.T) {
	saved := marketWSS
	defer func() { marketWSS = saved }()

	upgrader := websocket.Upgrader{
		ReadBufferSize:  4096,
		WriteBufferSize: 4096,
	}

	var req struct{ Id, Req string }
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		conn, _ := upgrader.Upgrade(w, r, nil)

		_, reader, _ := conn.NextReader()
		b, _ := ioutil.ReadAll(reader)
		json.Unmarshal(b, &req)

		wr, _ := conn.NextWriter(websocket.BinaryMessage)
		z := gzip.NewWriter(wr)
		z.Write([]byte(fmt.Sprintf(`
{
  "rep": %q,
  "status": "ok",
  "id": %q,
  "tick": {
    "amount": 12224.2922,
    "open":   9790.52,
    "close":  10195.00,
    "high":   10300.00,
    "ts":     1494496390000,
    "id":     1494496390,
    "count":  15195,
    "low":    9657.00,
    "vol":    121906001.754751
  }
}
`,
			req.Req,
			req.Id,
		)))
		z.Close()
		wr.Close()

		conn.Close()
	}))
	defer srv.Close()

	marketWSS = "ws://" + strings.TrimPrefix(srv.URL, "http://")
	streams, err := NewStreams()
	if err != nil {
		t.Error(err)
	}

	tick, err := streams.GetTick("btcusdt")
	if err != nil {
		t.Error(err)
	}

	testInt64 := func(a, b interface{}) bool {
		return a.(int64) == b.(int64)
	}
	testString := func(a, b interface{}) bool {
		return a.(string) == b.(string)
	}
	testFloat64 := func(a, b interface{}) bool {
		return math.Epsilon(1e-9).Cmp(a.(float64), b.(float64)) == 0
	}
	for _, check := range []struct {
		val  interface{}
		want interface{}
		test func(a, b interface{}) bool
		fmt  string
	}{
		{req.Req, "market.btcusdt.detail", testString, "Req = %q, want %q"},
		{tick.Id, int64(1494496390), testInt64, "tick.Id = %d, want %d"},
		{tick.Open, 9790.52, testFloat64, "tick.Open = %f, want %f"},
		{tick.Close, 10195.00, testFloat64, "tick.Close = %f, want %f"},
		{tick.High, 10300.00, testFloat64, "tick.High = %f, want %f"},
		{tick.Low, 9657.00, testFloat64, "tick.Low = %f, want %f"},
		{tick.Amount, 12224.2922, testFloat64, "tick.Amount = %f, want %f"},
		{tick.Count, int64(15195), testInt64, "tick.Count = %d, want %d"},
		{tick.Volume, 121906001.754751, testFloat64, "tick.Volume = %f, want %f"},
	} {
		if !check.test(check.val, check.want) {
			t.Errorf(check.fmt, check.val, check.want)
		}
	}
}
