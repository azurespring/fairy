package huobi

import (
	"encoding/json"
	"fmt"
)

type Step int

const (
	Step0 Step = iota
	Step1
	Step2
	Step3
	Step4
	Step5
)

type PriceLevel struct {
	Price, Quantity float64
}

func (l *PriceLevel) MarshalJSON() ([]byte, error) {
	return json.Marshal([]float64{l.Price, l.Quantity})
}

func (l *PriceLevel) UnmarshalJSON(data []byte) error {
	return json.Unmarshal(data, &[2]*float64{&l.Price, &l.Quantity})
}

type Depth struct {
	Asks []PriceLevel `json:"asks"`
	Bids []PriceLevel `json:"bids"`
}

type tDepth struct {
	symbol string
	step   Step
}

func (t tDepth) String() string {
	return fmt.Sprintf("market.%s.depth.step%d", t.symbol, t.step)
}

func (s *Streams) GetDepth(symbol string, step Step) (*Depth, error) {
	var d Depth
	if err := s.request(tDepth{symbol, step}, &d); err != nil {
		return nil, err
	}

	return &d, nil
}

type dxDepth chan<- *Depth

func (f dxDepth) get() interface{} {
	return &Depth{}
}

func (f dxDepth) put(v interface{}) interface{} {
	select {
	case f <- v.(*Depth):
		return v

	default:
		return nil
	}
}

func (f dxDepth) close() {
	close(f)
}

type DepthObserver struct {
	s  *Streams
	t  tDepth
	ch chan *Depth
}

func (o *DepthObserver) Chan() <-chan *Depth {
	return o.ch
}

func (o *DepthObserver) Close() {
	o.s.unsubscribe(o.t, dxDepth(o.ch))
}

func (s *Streams) ObserveDepth(symbol string, step Step) (*DepthObserver, error) {
	t := tDepth{symbol, step}
	ch := make(chan *Depth, 5)
	if err := s.subscribe(t, dxDepth(ch)); err != nil {
		return nil, err
	}

	return &DepthObserver{s, t, ch}, nil
}
