package huobi

import (
	"encoding/base64"
	"github.com/satori/go.uuid"
)

type command interface {
	exec(s *Streams) error
}

type request struct {
	t  topic
	ch chan<- wsBundle
}

func (cmd request) exec(s *Streams) error {
	id := newId()
	if err := write(s.conn, map[string]string{"id": id, "req": cmd.t.String()}); err != nil {
		cmd.ch <- wsBundle{nil, err}

		return err
	}

	s.reqs[id] = cmd.ch

	return nil
}

type subscription struct {
	t   topic
	ch  chan<- error
	dup duplex
}

func (cmd subscription) exec(s *Streams) error {
	topic := cmd.t.String()
	if brk, ok := s.brks[topic]; ok {
		brk.cs = append(brk.cs, cmd.dup)
		cmd.ch <- nil

		return nil
	}

	id := newId()
	if err := write(s.conn, map[string]string{"id": id, "sub": topic}); err != nil {
		cmd.ch <- nil

		return err
	}

	ch := make(chan wsBundle)
	s.reqs[id] = ch

	go func() {
		b := <-ch
		cmd.ch <- b.err
		if b.err == nil {
			s.brks[topic] = &broker{cmd.dup, []consumer{cmd.dup}}
		}
	}()

	return nil
}

type unsubscription struct {
	t topic
	c consumer
}

func (cmd unsubscription) exec(s *Streams) error {
	topic := cmd.t.String()
	brk, ok := s.brks[topic]
	if !ok {
		return nil
	}

	var cs []consumer
	for _, c := range brk.cs {
		if c != cmd.c {
			cs = append(cs, c)
		}
	}
	if len(cs) > 0 {
		brk.cs = cs

		return nil
	}
	delete(s.brks, topic)

	return write(s.conn, map[string]string{"id": newId(), "unsub": topic})
}

func newId() string {
	return base64.RawURLEncoding.EncodeToString(uuid.NewV1().Bytes())
}
