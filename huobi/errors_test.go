package huobi

import "testing"

func TestRestError_Error(t *testing.T) {
	e := RestError{"invalid-parameter", "invalid symbol"}
	if e.Error() != "invalid-parameter: invalid symbol" {
		t.Errorf("e.Error() = %q, want %q", e.Error(), "invalid-parameter: invalid symbol")
	}
}
